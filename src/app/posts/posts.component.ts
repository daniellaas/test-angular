import { Component, OnInit } from '@angular/core';
import { Posts } from './../interfaces/posts';
import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { ActivatedRoute } from '@angular/router';
import { Users } from '../interfaces/users';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts$: Posts[];
  users$: Users[];
  title:String;
  body:String;
  author:string;
  ans:string;
  constructor(private PostsService: PostsService) { }
  myFunc(){
    for (let index = 0; index < this.posts$.length; index++) {
      for (let i = 0; i < this.users$.length; i++) {
        if (this.posts$[index].userId==this.users$[i].id) {
          this.title = this.posts$[index].title;
          this.body = this.posts$[index].body;
          this.author = this.users$[i].name;
          this.PostsService.addPosts(this.body,this.author,this.title);   
        }  
      } 
    }
  this.ans ="The data retention was successful"
  }
  
  
  ngOnInit() {
    this.PostsService.getPosts()
      .subscribe(posts => this.posts$ = posts);
    this.PostsService.getUsers()
      .subscribe(users => this.users$ = users);
  }

}