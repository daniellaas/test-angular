import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  private url = "https://fwypn007g5.execute-api.us-east-1.amazonaws.com/beta";
  
  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  listclassCollection:AngularFirestoreCollection
  public doc:string; 
  
  classify():Observable<any>{
    console.log(this.doc);
    let json = {
      "articles": [
        {
          "text": this.doc
        },
      ]
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        let final = res.body.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;      
      })
    );      
  }

  addclass(userId:string, text:string, cat:string){
    const classified = {text:text,cat:cat}
    //this.db.collection('books').add(book)  
    this.userCollection.doc(userId).collection('classified').add(classified);
  }

  getlist(userId): Observable<any[]> {
    
    this.listclassCollection = this.db.collection(`users/${userId}/classified`);
    console.log('Books collection created');
    return this.listclassCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data;
      }))
    );    
  } 

  constructor(private http: HttpClient, private db: AngularFirestore,
    private authService:AuthService) { }
}