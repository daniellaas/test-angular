import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-listclass',
  templateUrl: './listclass.component.html',
  styleUrls: ['./listclass.component.css']
})
export class ListclassComponent implements OnInit {

  classifeds$:Observable<any[]>;
  userId:string;
  
  constructor(private ClassifyService:ClassifyService,
    public authService:AuthService) { }

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.classifeds$ = this.ClassifyService.getlist(this.userId); 
      }
    )
  }

}