import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { getMultipleValuesInSingleSelectionError } from '@angular/cdk/collections';
import { Posts } from './interfaces/posts';
import { Users } from './interfaces/users';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiUrl = 'https://jsonplaceholder.typicode.com/posts/';
  apiUrlUser = 'https://jsonplaceholder.typicode.com/users';


  constructor(private _http: HttpClient,private db:AngularFirestore) {
    
   }

   addPosts(body:String, author:String,name:String){
    const post = {body:body, author:author, name:name};
    this.db.collection('posts').add(post);
  }
   getPosts() {
    return this._http.get<Posts[]>(this.apiUrl);}
    getUsers() {
      return this._http.get<Users[]>(this.apiUrlUser);  
    }
}
