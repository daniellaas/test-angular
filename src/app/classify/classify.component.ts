import { Component, OnInit } from '@angular/core';
import { ClassifyService } from './../classify.service';
import { Router } from '@angular/router';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  url:string;
  text:string;
  onSubmit(){
    this.classifyService.doc = this.text;
    this.router.navigate(['/classified']);
  }
  constructor(private classifyService:ClassifyService, 
              public imageService:ImageService,
              private router:Router ) { }

  ngOnInit() {  
  }


}
