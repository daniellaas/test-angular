import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { ImageService } from '../image.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  public categories:object[] = [{id:0, name: 'business'},{id:1, name:'entertainment'}, {id:2, name:'politics'}, {id:3, name: 'sport'}, {id:4, name:'tech'}]
  categoryImage:string;
  
  userId:string;
  text:string; 
  cat:string;
  constructor(public classifyService:ClassifyService,
    public imageService:ImageService,
    private authService:AuthService,
    private router:Router,) { }

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid; }
    )
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classifyService.doc)
      }
    )

     

  }
  onSubmit(){
    console.log(this.userId); 
    //this.classifyService.addclass(this.userId,this.classifyService.doc,this.category)
    //this.router.navigate(['/listclass']);  
  } 
}
