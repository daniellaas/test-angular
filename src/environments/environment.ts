// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD90uwi_ogYcwK7YKNmeJcXhZTb7TsG4CM",
  authDomain: "test-9fa3c.firebaseapp.com",
  databaseURL: "https://test-9fa3c.firebaseio.com",
  projectId: "test-9fa3c",
  storageBucket: "test-9fa3c.appspot.com",
  messagingSenderId: "544495224692",
  appId: "1:544495224692:web:d4b13470e672700a700e21"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
